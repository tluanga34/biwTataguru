
(function(){
	
	var app = angular.module("targetAchievements",[]);

	app.controller("targetController",function($scope){
		$scope.campaign = dynamicData.campaign;
		
		$scope.campaignBox= (function(){
			
			if($scope.campaign.length > 3)
				return {
					"class" : "quarter",
					"showArrow" : true
				};
			else if($scope.campaign.length > 2)
				return {
					"class" : "quarter",
					"showArrow" : false
				};
			else if($scope.campaign.length < 2)
				return {
					"class" : "singleBox",
					"showArrow" : false
				};
			else
				return {
					"class" : "halfBox",
					"showArrow" : false
				};
			
		})();
		
	});
	
	
	var corousel = {
		
		"const" : function(){
			this.container = document.getElementById("campaign-corousel");
			this.items = document.getElementsByClassName("quarter");
			this.nextBtn = document.getElementById("campaign-corousel-next");
			this.prevBtn = document.getElementById("campaign-corousel-prev");
		},
		
		"boot" : function(){
			this.const();
			var itemWidth,
				containerWidth,
				leftPos = 0,
				screenWidth,
				parent = document.getElementById("carousel-occasional-lg"),
				max,
				debounce;
			
			updateDim();
			
			this.nextBtn.addEventListener("click",function(){
				if(leftPos > max)
				{
					leftPos -= itemWidth;
					$(corousel.container).css({"transform":"translate("+leftPos+"px,0px)"});
				}
			});
			
			this.prevBtn.addEventListener("click",function(){
				if(leftPos < 0)
				{
					leftPos += itemWidth;
					$(corousel.container).css({"transform":"translate("+leftPos+"px,0px)"});
				}
			});
			
			function updateDim()
			{
				screenWidth = document.documentElement.clientWidth;
				leftPos = 0;
				if(screenWidth > 767)
				{
					itemWidth =  corousel.items[0].offsetWidth;
					containerWidth = itemWidth * corousel.items.length;
					max = itemWidth * (corousel.items.length - 3) * -1;
					$(corousel.container).css({"width":containerWidth+"px"});
				}
				else
				{
					itemWidth = parent.clientWidth;
					containerWidth = itemWidth * corousel.items.length;
					$(corousel.items).css({"width": parent.clientWidth + "px"});
					max = itemWidth * (corousel.items.length - 1) * -1;
					$(corousel.container).css({"width": containerWidth+"px"});
				}
				clearTimeout(debounce);
				
				debounce = setTimeout(function(){
					$(corousel.container).css({"transform":"translate(0px,0px)"});
				},200);
				
			}
			
			window.addEventListener("resize",function(){
				updateDim();
			});
		}
	};
	
	window.addEventListener("load",function(){
		if(dynamicData.campaign.length > 2)
			corousel.boot();
	});
	
})();
