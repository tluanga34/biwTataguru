//CONSTRCTOR FOR PAGINATIONS
function Pagination(pages, currentIndx){
	this.pages = pages;
	this.currentIndx = (currentIndx == undefined)? 0 : currentIndx;
	this.length = pages.length;
	this.value = pages[this.currentIndx].items;
}

Pagination.prototype.next = function(){
	this.currentIndx ++;
	this.value = this.pages[this.currentIndx].items;
}

Pagination.prototype.prev = function(){
	this.currentIndx--;
	this.value = this.pages[this.currentIndx].items;
}


function Toggle(state){
	
	this.status = state;
	this.shown = state;
}

Toggle.prototype.show = function(){
	this.status = true;
	this.shown = true;
};

Toggle.prototype.hide = function(){
	this.status = false;
	this.shown = false;
};

Toggle.prototype.toggle = function(){
	this.status = !this.status;
	this.shown = !this.shown;
}

function SELECT_COUNTER(pages){
	this.selectedIds = [];
	
	this.count = function(){
		var self = this;
		self.selectedIds = [];
		pages.forEach(function(key){
			key.items.forEach(function(key1){
				if(key1.selected == true){
					self.selectedIds.push(key1.id);
				}
			});
		});
	}
}

function SORT_COLUMN(){
	this.order = "asc";
	this.key = '';
	this.sortIt = function(dir){
		
		if(dir == this.key){
			this.key = "-"+dir; //Sort by descending
			this.order = "dsc";
		} else {
			this.key = dir;
			this.order = "asc";
		}				
	}
}

//Full screen gallery Constructor
function FSGallery(data){
	this.imgUrl = "";
	this.data = [];
	this.currentIndx = 0;
	this.shown = false;
	
	if(data != undefined)
		this.data = data;
}

FSGallery.prototype.show = function(indx){
	this.shown = true;
	if(indx != undefined)
		this.currentIndx = parseInt(indx);
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};

FSGallery.prototype.hide = function(){
	this.shown = false;
};

FSGallery.prototype.next = function(){
	var len = this.data.length;
	this.currentIndx = (this.currentIndx >= (len-1))? 0 : ++this.currentIndx;
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};

FSGallery.prototype.prev = function(){
	var len = this.data.length;
	this.currentIndx = (this.currentIndx <= 0)? (len-1) : --this.currentIndx;
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};


var app = angular.module("app",[]);

app.service("redirectTimer",function($interval){
	this.construct = function(sec, url){
		this.sec = sec;
		this.url = url;
	}
	
	this.redirect = function(){
		var self = this;
		window.location.href = self.url;
	}
	
	this.start = function(){
		var self = this;
		$interval(function(){
			if(self.sec <= 0){
				window.location.href = self.url;
			}
			else
				self.sec--;
		}, 1000);
	}
});

app.controller("view",function($scope, $http){
	var s = $scope;
	
	s.albums = dynamicData;
	s.gallery = new Gallery(false, s.albums);


	s.finalSubmit = {
		prompt 		: new Toggle(false),
		loadingAnim : new Toggle(false),
		errorMsg 	: new Toggle(false),
		finalMsg 	: new Toggle(false),
		submit 		: function(){

			//Ajax call to a server telling that user has confirmed his/her submission.
			var self = this;
			self.loadingAnim.show();
			self.errorMsg.hide();
			$http({
				url : "http://172.16.174.98/workspace/PROJECTS/Tata_motors/GURU/Tata-guru--v5html/data/submit.php"
			}).then(function(sucess){
				self.finalMsg.show();
				self.prompt.hide();
				self.loadingAnim.hide();
			},function(){
				self.loadingAnim.hide();
				self.errorMsg.show();
			});
		}
	};
	
	function Gallery(defaultState, data){
		
		var currentIndex = 0,
			len,
			self = this;

		self.item = [];
		
		self.showing = defaultState;
		
		self.show = function(itemIndex){
			
			self.currentImgUrl = self.item[parseInt(itemIndex)].imgUrl;
			self.showing = true;
			currentIndex = itemIndex;
		}
		self.hide = function(){
			self.showing = false;
		}
		

		data.forEach(function(key){
			self.item.push({
				imgUrl : key.imgUrl
			});
		});

		len = this.item.length;
		
		this.currentImgUrl = this.item[currentIndex].imgUrl;
		
		this.next = function(){

			currentIndex = (currentIndex == (len-1))? 0 : ++currentIndex;
			this.currentImgUrl = this.item[currentIndex].imgUrl;
		}
		
		this.prev = function(){
			currentIndex = (currentIndex == 0)? len-1 : --currentIndex;
			this.currentImgUrl = this.item[currentIndex].imgUrl;
		}
	
	}
	
	
	s.stopPropagation = function(e){e.stopPropagation();}
	
});

app.controller("upload",function($scope){
	
	var s = $scope;
	
	s.albums = new Albums(dynamicData);
	
	
	
	function Albums(dynamicData){

		var self = this;
		self.item = [];
		
		dynamicData.forEach(function(key,indx){
			self.item.push(new Items(dynamicData[indx]));
		});
				
		var hiddenForm = document.getElementById("hiddenForm");
		
		this.submit = function(){
			hiddenForm.submit();
		}
		
		function Items(obj){
			this.imgUrl = obj.imgUrl;
			this.uploaded = obj.uploaded;
			this.theme = obj.theme;
			hiddenForm = document.getElementById("hiddenForm");
			
			this.fileInput = document.createElement("input");
			this.fileInput.type = "file";
			this.fileInput.accept = "image/*";
			this.fileInput.name = obj.name;

			hiddenForm.appendChild(this.fileInput);
		}
		
		Items.prototype.delete = function(){
			this.imgUrl = "";
			this.uploaded = false;
		};
		
		Items.prototype.addImage = function(){
			var self = this,
				form = hiddenForm,
				input = self.fileInput;
			
			input.click();
			
			input.addEventListener('input',function(evt){
				var files = evt.target.files;
				var f = files[0];
				var reader = new FileReader();
				 
				reader.onload = (function(theFile) {
					return function(e) {						
						self.imgUrl = e.target.result;
						$scope.$digest();
					}
				})(f);
				 
				reader.readAsDataURL(f);
			});
		}
	}

});




app.controller("regionWise",function($scope, $http){
	
	var s = $scope;
	
	s.data 		= new Data(dynamicData);
	s.loading 	= new Toggle(false);
	s.errorMsg 	= new Toggle(false);
	s.config	= dynamicData.config;

	function Data(dynamicData){
		
		this.region = dynamicData.region;
		
		this.construct = function(data){
			this.items = data.pages;
			this.pages = new Pagination(this.items, data.currentPage);			
		}
		
		this.construct(dynamicData);
		
		this.changeRegion = function(region){
			
			var self = this;
			
			s.errorMsg.hide();
			$scope.loading.show();
			
			$http({
				url : s.config.api.changeRegion,
				params : {
					"region" : region
				}
			}).then(
				function(sucess){
					self.region = region;
					self.construct(sucess.data);
					$scope.loading.hide();
				},
				function(failed){
					s.errorMsg.show();
					$scope.loading.hide();
				}
			);
		}
		
		this.sort = new SORT_COLUMN();
		this.sort.sortIt("dist_name");
	}	
});

app.controller("distributors",function($scope, $http, redirectTimer){
	
	var s = $scope;
	
	s.data 				= new Data(dynamicData);
	s.fsGallery 		= new FSGallery();
	s.stopPropagation 	= function(e){e.stopPropagation();}
	s.loading 			= new Toggle(false);
	s.errorMsg 			= new Toggle(false);
	s.successMsg 		= new Toggle(false);
	s.warningMsg 		= new Toggle(false);
	s.config			= dynamicData.config;
	s.timer 			= redirectTimer;
	
	s.timer.construct(5,dynamicData.redirectToAfterSave);
	
	function Data(dynamicData){
		
		this.construct = function(data){
			this.items 		= data.pages;
			this.dist_name 	= data.dist_name;
			this.id 		= data.id;
			this.area 		= data.area;
			this.pages 		= new Pagination(this.items, data.currentPage);
			this.counter 	= new SELECT_COUNTER(this.items);
		}
					
		this.shortList = function(){
			var self = this;
				selectedIds = self.counter.selectedIds,
			

			$scope.warningMsg.hide();
			$scope.errorMsg.hide();
			$scope.successMsg.hide();
			
			$scope.loading.show();
			
			$http({
				url 	: dynamicData.config.api.shortlist,
				method 	: "POST",
				params : {
					id : self.id
				},
				data 	: selectedIds,
				headers	: {
					'Content-Type': 'application/x-www-form-urlencoded',
				}
			}).then(
				function(sucess){
					
					self.items.forEach(function(key){
						key.shortListed = key.selected;
					});
					
					$scope.loading.hide();
					$scope.successMsg.show();
					
					$scope.timer.start();
				},
				function(failed){
					console.log(failed);
					$scope.loading.hide();
					$scope.errorMsg.show();
				}
			);
		}
		
		this.construct(dynamicData);
	}

});

app.controller("declare_winners",function($scope, $http, redirectTimer){
	
	var s = $scope;
	
	s.data 			= new Data(dynamicData);
	s.fsGallery 	= new FSGallery();
	s.stopPropagation = function(e){e.stopPropagation();}
	s.loading 		= new Toggle(false);
	s.errorMsg 		= new Toggle(false);
	s.successMsg 	= new Toggle(false);
	s.warningMsg	= new Toggle(false);
	s.savedMsg 		= new Toggle(false);
	s.timer 		= redirectTimer;
	
	s.timer.construct(5,dynamicData.redirectToAfterSave);
	
	function Data(dynamicData){
		
		this.construct = function(data){
			this.region = data.region;
			this.items = data.pages;
			this.pages = new Pagination(this.items, data.currentPage);
			this.counter = new SELECT_COUNTER(this.items);
		}

		this.declareWinner = function(){
			
			var self = this,
				selectedIds = self.counter.selectedIds;
						
			s.warningMsg.hide();
			s.errorMsg.hide();
			s.successMsg.hide();
			s.savedMsg.hide();
			s.loading.show();
			
			$http({
				url : dynamicData.config.api.declareWinner,
				params : {
					region : self.region
				},
				data : selectedIds,
				method : "POST",
				headers	: {
					'Content-Type': 'application/x-www-form-urlencoded',
				}
			}).then(
				function(sucess){
					self.winnerId = self.selectedId;
					
					self.items.forEach(function(key){
						key.items.forEach(function(key1){
							if(key1.selected == true){
								key1.declaredWinner = true;
							} else {
								key1.declaredWinner = false;
							}
						});
					});
					
					s.loading.hide();
					s.successMsg.show();
				},
				function(failed){
					s.loading.hide();
					s.errorMsg.show();
				}
			);
		}
		
		this.updateSelection = function(){
			
			var self = this,
				selectedIds = self.counter.selectedIds;
						
			s.warningMsg.hide();
			s.errorMsg.hide();
			s.successMsg.hide();
			s.savedMsg.hide();
			s.loading.show();
			
			$http({
				url : dynamicData.config.api.updateSelection,
				params : {
					region : self.region
				},
				data : selectedIds,
				method : "POST",
				headers	: {
					'Content-Type': 'application/x-www-form-urlencoded',
				}
			}).then(
				function(sucess){
					s.loading.hide();
					s.savedMsg.show();
					s.timer.redirect();
				},
				function(failed){
					s.loading.hide();
					s.errorMsg.show();
				}
			);
		}

		this.changeRegion = function(region){
			
			var self = this;
			
			s.warningMsg.hide();
			s.errorMsg.hide();
			s.successMsg.hide();
			s.savedMsg.hide();
			$scope.loading.show();
			
			$http({
				url : dynamicData.config.api.changeRegion,
				params : {
					"region" : region
				}
			}).then(
				function(sucess){
					self.region = region;
					self.construct(sucess.data);
					$scope.loading.hide();
				},
				function(failed){
					s.errorMsg.show();
					$scope.loading.hide();
				}
			);
		}
				
		this.construct(dynamicData);
	}
});

//CONTROLLER FOR THE WINNER PAGE
app.controller("winner",function($scope, $http){
	
	var s = $scope;
	s.loading = new Toggle(false);
	s.errorMsg = new Toggle(false);
	s.data = new Data(dynamicData);

	console.log(s.data.page);
	
	function Data(dynamicData){
		
		var self = this;
		self.region 	= dynamicData.region;
		
		self.construct = function(champions){

			self.champ = [];
			
			champions.forEach(function(key,indx){
				self.champ.push(new Champoins(key));
			});
			
			self.page = new Pagination(self.champ);
			
		};
		
		self.construct(dynamicData.champions);
		
		function Champoins(dataItems){
			this.champ_Id 	= dataItems.champ_Id;
			this.champ_name = dataItems.champ_name;
			this.shop_name 	= dataItems.shop_name;
			this.dist_name 	= dataItems.dist_name;
			this.area 		= dataItems.area;
			this.gallery 	= new Gallery(dataItems.images);
		}
		
		function Pagination(champ){
			this.currentIndx = 0;
			this.length = champ.length;
			this.value = champ[this.currentIndx];
			
			this.next = function(){
				this.currentIndx ++;
				this.value = champ[this.currentIndx];
			}
			
			this.prev = function(){
				this.currentIndx--;
				this.value = champ[this.currentIndx];
			}
		}
		
		this.changeRegion = function(region){
			
			$scope.loading.show();
			$scope.errorMsg.hide();
			
			$http({
				url : "http://172.16.174.75/workspace/PROJECTS/Tata_motors/Tata_champoin/data/getRegionalWinner.php?",
				params : {region:region},
				method : "GET"
			}).then(
				function(sucess){
					$scope.loading.hide();
					self.region = region;
					console.log(sucess.data);
					self.construct(sucess.data.champions);
				},
				function(failed){
					$scope.loading.hide();
					$scope.errorMsg.show();
				}
			)
		};
		
		function Gallery(images){

			var currentIndx = 0;
			
			this.construct = function(images){
				currentIndx = 0;
				this.items = images;
				this.imgUrl = this.items[currentIndx].imgUrl;
				this.items[currentIndx].active = true;
			}
			
			this.construct(images);

			this.changeImage = function(indx){				
				if(indx != undefined){
					this.items[currentIndx].active = false;	
					currentIndx = parseInt(indx);
					this.imgUrl = this.items[currentIndx].imgUrl;
					this.items[currentIndx].active = true;
				}				
			}
			
			this.next = function(){
				var len = this.items.length;
				this.items[currentIndx].active = false;
				currentIndx = (currentIndx >= (len-1))? 0 : ++currentIndx;
				this.imgUrl = this.items[currentIndx].imgUrl;
				this.items[currentIndx].active = true;
			};
			this.prev = function(){
				var len = this.items.length;
				this.items[currentIndx].active = false;
				currentIndx = (currentIndx <= 0)? (len-1) : --currentIndx;
				this.imgUrl = this.items[currentIndx].imgUrl;
				this.items[currentIndx].active = true;
			};
		}
	}
});


app.directive("ngGallery",function(){
	return {
		scope : {
			ngGallery : "=",
			ngGalleryControl : "="
		},
		link : function(scope, elem, attr){
						
			var container 	= elem[0],
				data 		= scope.ngGallery,
				width		= container.clientWidth,
				eachWidth	= (width / data.length) - 1,
				widthPerc 	= (100 / data.length) - 0.3,
				difference 	= 10,
				expndPerc	= widthPerc + difference,
				squeezePerc	= (100 - expndPerc) / (data.length - 1),
				li,
				fsGallery = scope.ngGalleryControl;
			
			data.forEach(function(key,indx){
				li = document.createElement("li");
				$(li).attr({"data-id":indx});
				$(li).css({"background-image":"url('"+key.imgUrl+"')", "width":widthPerc+"%"});
				container.appendChild(li);
			});
			
			$(container.children).on("mouseover",function(){
				$(this).css({"width":expndPerc+"%"});
				$(container.children).not(this).css({"width":squeezePerc+"%"});
			});
			
			$(container).on("mouseout",function(){
				$(this.children).css({"width":widthPerc+"%"});
			});
			
			$(container.children).on("click",function(){
				var currentIndx = $(this).attr("data-id");
				fsGallery.data = data;	
				fsGallery.show(currentIndx);
				scope.$apply();
			});
		}
		
	};
});
