app.controller("referal",["$scope","$http",function(s,$http){
	
	s.log = function(e){console.log(e)};
	
	function Tab(isReferalLead){
		this.isReferalLead = isReferalLead;
		this.toggle = function(){
			this.isReferalLead = !this.isReferalLead
		}
	}
	
	function Form(name){
		
		//Flag to check if number has been searched by click on "check validity" button.
		var numberSearched = false,
			_selfForm		= this;
		
		this.mobile 			= new Input("referalStatus");
		this.role 				= new Input("referalStatus");
		this.referalStatus 		= new Input("referalStatus");
		this.distributorCode	= new Input("distributorCode");
		this.distributor 		= new Input("distributor",this.distributorCode);
		this.apm				= new Input("apm", this.distributor);
		this.region 			= new Input("region", this.apm);
		this.invalidReason		= new Input("invalidReason");
		this.formName			= name;
		
		//ENABLE THE FORM SO THAT USER CAN START INPUT THE FORM IF PHONE NUMBER IS NOT FOUND IN EXISTING DATA.
		this.setAsValid = function(){
			this.role.enable();
			this.distributor.enable();
			this.distributor.setOptions({});
			this.distributorCode.enable();
			this.distributorCode.setOptions({});
			this.apm.enable();
			this.apm.setOptions({});
			this.region.enable();
			this.referalStatus.setOptions(dynamicData.status);
			this.referalStatus.setExistingDropdownValue("Valid");
			this.role.setOptions(dynamicData.role);
			this.region.setOptions(dynamicData.region);
			//this.invalidReason.enable();
			this.invalidReason.reset();
			this.submitted.hide();
		}
		
		//RESET ON CLICK ON RESET BUTTON AFTER DATA SUBMITTED.
		this.reset = function(){
			this.mobile.enable();
			this.role.reset();
			this.distributor.reset();
			this.distributorCode.reset();
			this.apm.reset();
			this.region.reset();
			this.invalidReason.reset();
			this.role.reset();
			this.region.reset();
			this.invalidReason.reset();
			this.submitted.hide();
		}
		
		//DISABLE THE FORM AFTER DATA SUBMITTED.
		this.disable = function(){
			this.mobile.valid = false;
			this.role.disable();
			this.distributor.disable();
			this.distributorCode.disable();
			this.apm.disable();
			this.region.disable();
			this.invalidReason.disable();
			this.role.disable();
			this.region.disable();
			this.invalidReason.disable();
		}
		
		//CONSTRCTOR FOR INPUT
		function Input(keyname, childTofeed){
			this.valid 		= false;
			this.keyname 	= keyname;
			this.disabled 	= true;
			this.value 		= "";
			this.options 	= [];
			this.toFeed 	= (childTofeed != undefined)?childTofeed : null;
		}
		
		Input.prototype.checkValidity = function(e){
			var mobile = this.value;
			
			numberSearched = false;
			
			if(mobile <= 9999999999 && mobile >= 1000000000)
				this.valid = true;
			else{
				this.valid = false;
			}
		}
		
		Input.prototype.enable = function(){
			this.value = "";
			this.disabled = false;
		}
		
		Input.prototype.disable = function(){
			this.disabled = true;
		}
		
		Input.prototype.setExistingDropdownValue = function(value){
			this.options 	= [{name : value}];
			this.value 		= this.options[0];
			this.disabled	= true;
		}
		
		Input.prototype.setExistingValue = function(value){
			this.value 		= value;
			this.disabled	= true;
		}
		
		Input.prototype.setOptions = function(key){
			this.options = key;
		}
		
		Input.prototype.reset = function(){
			this.value = "";
			this.disabled = true;
		}
		
		//FUNCTION FOR GETTING THE FORM OPTIONS FOR THE NEXT FORM ON INPUT CHANGE EVENT
		Input.prototype.getOptions = function(){

			var self = this;
			
			if(self.toFeed == null){
				return;
			}
			
			var data = {
				match 	: self.keyname,
				value	: self.value
			}
			
			console.log("\n\nBelow is the sample of Data being post to the server\nMatch key will specify which key to match. i.e Region or APM. \nValue has name and id which can be used as a search term in the server");
			console.log(data);
			
			s.loading.show();
			
			$http({
				url : s.api.getFormOption,
				data : data,
				method : "POST",
				headers	: {
					'Content-Type': 'application/json',
				}
			}).then(
				function(sucess){
					console.log("\n\nData Returned from Server");
					console.log(sucess.data);
					s.loading.hide();
					
					self.toFeed.setOptions(sucess.data);
					
					function resetaToFeed(toFeed){
						if(toFeed){
							console.log(toFeed);
							toFeed.enable();
							
							if(toFeed.toFeed){
								resetaToFeed(toFeed.toFeed);
							}
						}
					}
					resetaToFeed(self.toFeed);
				},
				function(failed){
					s.loading.hide();
				}
			);
		}
		
		//THIS METHOD PRE-POPULATE THE EXISTING DATA FROM SERVER TO THE INPUT FORM.
		this.setExistingData = function(e){
			this.invalidReason.setExistingValue(e.invalidReason);
			this.role.setExistingDropdownValue(e.role);
			this.referalStatus.setExistingDropdownValue(e.referalStatus);
			this.distributor.setExistingDropdownValue(e.distributor);
			this.distributorCode.setExistingDropdownValue(e.distributorCode);
			this.apm.setExistingDropdownValue(e.apm);
			this.region.setExistingDropdownValue(e.region);
			this.invalidReason.disabled = false;
			this.submitted.hide();
		};
		
		//THIS METHOD SUBMIT DATA TO SERVER BY CLICK ON SUBMIT BUTTON.
		this.submit = function(e){
			
			var self = this;
			e.preventDefault();
			
			//SUBMIT DATA ONLY WHEN NUMBER HAS BEEN SEARCHED NO MATTER IF ITS EXIST OR NOT.
			if(numberSearched === true) {
				console.log("Number Searched form. Start ajax call");
				this.notSearchedMsg.hide();
				
				var data = {
					role 			: this.role.value.name,
					referalStatus 	: this.referalStatus.value.name,
					distributor		: this.distributor.value.name,
					apm				: this.apm.value.name,
					distributorCode	: this.distributorCode.value.name,
					region			: this.region.value.name,
					invalidReason	: this.invalidReason.value,
					formName		: this.formName
				}
				
				console.log(data);
				s.loading.show();
				
				$http({
					url : s.api.submitForm,
					data : data,
					method : "POST",
					headers	: {
					'Content-Type': 'application/json',
					}
				}).then(
					function(sucess){
						console.log(sucess.data);
						self.submitted.show();
						self.disable();
						s.loading.hide();
					},
					function(failed){
						console.log(failed);
						s.loading.hide();
					}
				);
				
				//NUMBER IS NOT SEARCHED. SHOW "PLEASE VALIDATE" MESSAGE.
			} else {
				console.log("Number Not Searched");
				this.notSearchedMsg.show();
			}
		};
		
		//THIS Function SEARCH FOR  A DATA WHICH IS ALREADY EXIST IN THE SYSTEM BY PHONE NUMBER
		this.searchExisting = function(e){
			var self = this,
				mobile = this.mobile.value;
						
			if(mobile <= 9999999999 && mobile >= 1000000000)
				search();
			
			self.notSearchedMsg.hide();
			
			function search(){
				s.loading.show();
				$http({
					url : s.api.searchExisting,
					params : {
						mobile : mobile
					}
				}).then(
					function(sucess){
						//console.log(sucess.data);
						if(sucess.data.status == "notFound"){
							console.log("Not found");
							self.setAsValid();
						}
						else
							self.setExistingData(sucess.data);
						
						s.loading.hide();
						numberSearched = true;						
					},
					function(failed){
						s.loading.hide();
					}
				);
			}
		}
		
		//INITIATING OBJETS.
		this.mobile.disabled = false;
		this.role.setOptions(dynamicData.role);
		this.region.setOptions(dynamicData.region);
		this.referalStatus.setOptions(dynamicData.status);
		this.notSearchedMsg = new Toggle(false);
		this.submitted 	= new Toggle(false);
	}
	
	s.tab 			= new Tab(dynamicData.config.isReferalLead);
	s.referalForm 	= new Form("ReferalForm");
	s.regnForm 		= new Form("RegnForm");
	s.api 			= dynamicData.config.api;
	s.loading 		= new Toggle(false);
}]);