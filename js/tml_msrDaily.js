app.controller("dsrDaily",["$scope","$http",function(s,$http){
	

	function Form(){

		var _selfForm = this;

		//CONSTRUCTOR FOR ALL TEXT BOXES.
		function Textbox(val){
			if(val != undefined)
			this.value = val;
			this.disabled = false;		
		}
		Textbox.prototype.disable = function(){
			this.disabled = true;
		};
		Textbox.prototype.empty = function(){
			this.value = '';
		}
		Textbox.prototype.enable = function(){
			this.disabled = false;
		};
		Textbox.prototype.val = function(e){
			if(e == undefined)
				return this.value;
			else
				this.value = e;
		}

		//CONSTRUCTOR FOR MOBILE || MEMBER ID CHECK
		function MemberId(){
			this.alert = new Toggle(false);
			//this.value = 9738236953;
			this.value = '';
			this.validate = function(evt){

				//If keycode is not enter key, then return.
				if(evt != undefined && evt.keyCode != 13){
					return;
				}

				if(evt != undefined)
					evt.preventDefault();

				console.log(this.value); 

				_selfForm.formError.hide();

				if(this.value == '' || this.value == undefined) {
					this.alert.show("Please fill the above field");
					return;
				}

				var _self = this;
				s.loading.show();
				$http({
					url : s.api.checkMobile,
					params : {
						id : _self.value
					},
					method : 'GET'
				}).then(
					function(success){
						//console.log(success.data);

						if(success.data != null){
							_selfForm.name.val(success.data.name);							//Populate the name field
							_selfForm.prevDate.val(success.data.prevDate);					//Populate the Previous visit date field
							_selfForm.prevFeedback.val(success.data.prevFeedback || '');	//Populate the Previous feedback field
							_selfForm.enableAll();											//Enable all forms.
							_selfForm.emptyNonAutoFields();									//Reset values of non-autopopulated fields
							_self.alert.hide();												//Hide the alert messge if displayed.
							//IF THERE IS NO PREVIOUS FEEDBACK
							if(success.data.prevDate == undefined || success.data.prevDate == '' || success.data.prevDate == null){
								console.log("No Previous Feedback");
							} else{
								_selfForm.visitDate.initiate();
							}
							//_selfForm.name.val(success);
						} else{

							//BACKEND IS RETURING NULL VALUE IF ID IS NOT REGISTERED AT ALL.
							console.log("Null Returned");
							_self.alert.show("Invalid member Id / Mobile number");
							_selfForm.disableAll();
							_selfForm.emptyAll();
						}

						s.loading.hide();
					},
					function(failed){
						console.log(failed.data);
						s.loading.hide();
					}
				);
			}
			
			this.empty = function(){
				this.value = '';
			}
		}


		//CONSTRUCTOR FUNCTION FOR THE MULTISELECTOR FORM
		function Multiselect(apiUrl, mode){
			this.list 				= [];
			this.values 			= [];
			this.itemsListLinear 	= [];
			this.apiUrl 			= apiUrl || '';
			this.childNode			= null;
			this.parentNode 		= null;
			this.disabled 			= false;
			this.expanded 			= (mode == undefined)?true : mode;

			this.expand 	= function(){this.expanded = true}
			this.collapse 	= function(){this.expanded = false}

			//GET DYNAMIC DATA FOR MULTISELECT ITEMS
			this.get = function(data){

				var _self = this;

				s.loading.show();

				$http({
					url : _self.apiUrl,
					data : data || [],
					method : "POST",
					headers : {
						'Content-Type' : 'aplication/json'
					}
				}).then(
					function(success){
						_self.list = success.data;		//Assign dynamic data items to the list array.
						_self.values = [];				//Reset the value
						_self.itemsListLinear 	= [];
						s.loading.hide();				//Hide loading animation
						//_self.expand();
						if(_self.parentNode != null)
							_self.parentNode.collapse();
						// console.log(_self);

						if(_self.childNode != null)
							_self.childNode.resetValues();

						//Create a linear list array to feed in the autocomplete search box.
						_self.list.forEach(function(key){
							if(key.items != undefined) {
								_self.itemsListLinear = _self.itemsListLinear.concat(key.items);
							}
						});

					},
					function(failed){
						s.loading.hide();
					}
				);
			}

			//WHEN USER SELECT THE ITEM BY CLIKING ON THE ITEMS
			this.select = function(x){

				if(x.selected == true)
					return;

				this.values.push(x);
				x.selected = true;
				//console.log(x);
			}

			//WHEN USER DELETE THE ITEM BY CLICKING THE X BUTTON
			this.deselect = function(index){
				this.values[index].selected = false;
				this.values.splice(index, 1);
				//console.log(index);
			}

			this.disable = function(){
				if(!this.disabled)
					this.disabled = true;
			}

			this.enable = function(){
				if(this.disabled)
					this.disabled = false;
			}

			this.resetValues = function(){
				this.values = [];
				this.list = [];
				this.collapse();
			}

			//RESET ALL SELECTED FLAGS KEYS ASSIGNED ON THE ITEMS
			this.removeSelected = function(){
				this.list.forEach(function(key){
					if(key.selected)
						key.selected = false;
				});
			}

			this.expand = function(){this.expanded = true};
			this.collapse = function(){this.expanded = false};
			this.toggleExpansion = function(){this.expanded = !this.expanded};

		}


		//Function to disable all inputs
		this.disableAll = function(){
			this.visitDate.disable();
			this.memberFeedback.disable();
			this.shelfSpace.disable();
			this.feedbackCat.disable();
			this.promoMat.disable();
			this.promoLit.disable();
			this.partShared.disable();
			this.productGroup.disable();
		}

		//Function to empty all inputs
		this.emptyAll = function(){
			this.name.empty();
			this.prevFeedback.empty();
			this.prevDate.empty();
			this.visitDate.empty();
			this.memberFeedback.empty();
			this.shelfSpace.values = "";
			this.feedbackCat.values = "";
			//this.promoMat.empty();
			//this.promoLit.empty();
			//this.partShared.empty();
			this.productGroup.values = [];
			this.productLine.resetValues();
			this.partNo.resetValues();
			//this.productGroup.expand();
			this.productGroup.removeSelected();
		}

		//Funtion to empty all inputs which are not auto-populated fields
		this.emptyNonAutoFields = function(){
			this.visitDate.empty();
			this.memberFeedback.empty();
			this.shelfSpace.values = "";
			this.feedbackCat.values = "";
			//this.promoMat.empty();
			//this.promoLit.empty();
			//this.partShared.empty();
			this.productGroup.values = [];
			this.productLine.resetValues();
			this.partNo.resetValues();
			//this.productGroup.expand();
			this.productGroup.removeSelected();
		}
		
		//Function to enable all inputs
		this.enableAll = function(){
			this.productGroup.removeSelected();
			this.visitDate.enable();
			this.memberFeedback.enable();
			this.shelfSpace.enable();
			this.feedbackCat.enable();
			this.promoMat.enable();
			this.promoLit.enable();
			this.partShared.enable();
			this.productGroup.enable();
		}


		//Start constructing input objects
		this.id 				= new MemberId();
		this.name 				= new Textbox();
		this.prevFeedback 		= new Textbox();
		this.prevDate	 		= new Textbox();
		this.visitDate	 		= new Textbox();
		this.memberFeedback	 	= new Textbox();
		
		this.promoMat			= new Textbox(true);
		this.promoLit			= new Textbox(true);
		this.partShared			= new Textbox(false);

		//Construct the multiselect objects with the api url passed here.
		//Parameter 1 = API URL
		//Paremeter 2 = If it should expanded or not.
		this.shelfSpace			= new Multiselect('',true);
		this.feedbackCat		= new Multiselect('',true);
		this.productGroup		= new Multiselect(s.api.getProductGroup, false);
		this.productLine		= new Multiselect(s.api.getProductLine, false);
		this.partNo				= new Multiselect(s.api.getPartNo, false);

		//Define the child nodes for the multiselect forms.
		this.productGroup.childNode		= this.productLine;
		this.productLine.childNode 		= this.partNo;

		//Define the parent node for the multiselect forms.
		this.productLine.parentNode		= this.productGroup;
		this.partNo.parentNode			= this.productLine;

		/*
			productGroup | <-----> productLine | <-------> PartNo
	Null <--------parent | <-----------Parent  | <-------- Parent
				   child ------->       child  -------->    child ---> Null
		*/


		//Function declared particularly for vistDate object/
		//This function is called from inside directive and controller as well.
		//Initiate the date element as per needed
		this.visitDate.initiate = function(dateElem){
			if(dateElem)
				this.dateElem = dateElem;
			
			//Take tomorrow
			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate());

			//Take day from three days go
			var yesterDays = new Date();
			yesterDays.setDate(yesterDays.getDate() - 3);

			var sensitiveFrom = yesterDays.getTime();

			if(_selfForm.prevDate.value != "")
				var sensitiveFrom = Math.max(sensitiveFrom, _selfForm.prevDate.value);
			
			this.dateElem.setSensitiveRange(new Date(sensitiveFrom), tomorrow);
		}

		//INITIATE THE PROGRAM
		this.disableAll();
		this.productGroup.get();
		this.shelfSpace.list 	= dynamicData.options.shelfSpace;
		this.feedbackCat.list 	= dynamicData.options.feedbackCategories;
		this.shelfSpace.values 	= '';
		this.feedbackCat.values = '';

		//FORM VALIDATE MESSAGES//
		this.formError = new Toggle(false);

		//SUBMIT THE FORM HERE
		//VALIDATE INPUT DATA, GATHER ALL THE NECESSARY DATA AND SUBMIT IT TO SERVER.
		this.submit = function(evt){
			evt.preventDefault();

			//console.log(this);

			//VALIDATE START FOR FORM BEFORE SUBMIT//
			if(typeof(this.visitDate.value) != "object") {
				this.formError.show("Incomplete form: Please Check the ID");
				return;
			}

			//IF PART KNOWLEDGE SHARED IS YES BY USER
			if(this.partShared.value == true) {
				//PRODUCT GROUP VALUES CAN'T BE EMPTY
				if(this.productGroup.values.length <= 0){
					this.formError.show("Incomplete form: Please select item in the Product Groups field");
					return;
				}
				//PRODUCT LINE VALUES CAN'T BE EMPTY
				if(this.productLine.values.length <= 0){
					this.formError.show("Incomplete form: Please select item in the Product Line field");
					return;
				}
			}

			//VALIDATION END

			//GATHERING FORM DATA HERE
			var formData = {
				id : this.id.value,
				visitDate : this.visitDate.value.getTime(),
				shelfSpace : this.shelfSpace.values,
				promoMat : this.promoMat.value,
				promoLit : this.promoLit.value,
				partShared : this.partShared.value,
				feedbackCat : this.feedbackCat.values,
				memberFeedback : this.memberFeedback.value,
				productGroup : this.productGroup.values,
				productLine : this.productLine.values,
				partNo : this.partNo.values
			}

			console.log(formData);

			s.loading.show();

			//SEND GATHERED DATA TO SERVER
			$http({
				url : s.api.formAction,
				data : formData,
				method : "POST",
				headers : {
					'Content-Type' : "application/json"
				}
			}).then(
				function(success){
					//console.log(success.data);
					s.loading.hide();
					s.postSubmit.show();
				},
				function(failed){
					s.loading.hide();
				}
			);
		}

		//INITIATE RESET THE FORM WHEN USER
		this.reset = function(){
			this.disableAll();
			this.emptyAll();
			this.formError.hide();
			this.id.empty();
			$("html").velocity("scroll",{offset:"10px"});
		}
	}


	s.api 			= dynamicData.config.api;
	s.loading 		= new Toggle(false);
	s.postSubmit 	= new Toggle(false);
	s.form 			= new Form();


}]);