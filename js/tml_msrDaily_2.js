app.controller("MsrDaily_v2", ["$scope", "dataModel", "$http", function (scope, dataModel, $http) {

    var s = scope;
    var pg_cache = null;

    s.api = dynamicData.config.api;
    s.log = console.log;


    s.loading = new dataModel.Construct(false);
    s.postSubmit = new dataModel.Construct(false);
    s.formMessage = new dataModel.Construct(false);

    //scope.history = dynamicData.history;

    var temp;

    scope.pushRow = function (champion) {

        temp = angular.copy(champion);
        temp.champion_basics = champion;

        temp.feedback_categories = (new dataModel.Construct());
        temp.product_group = (new Multiselect(s.api.getProductGroup, false, "product_group"));
        temp.part_line = (new Multiselect(s.api.getProductLine, false, "part_line"));
        temp.part_no = (new Multiselect(s.api.getPartNo, false, "part_no"));
        temp.part_knowledge_shared = (new dataModel.Construct());
        temp.handedover_product_literature = (new dataModel.Construct());
        temp.does_retailer_get_sufficient_meterial = (new dataModel.Construct());
        temp.counter_share = (new dataModel.Construct());

        //temp.part_description = "";

        temp.feedback_text = "";


        temp.visit_date = new VisitDate();

        temp.feedback_categories.setList(dynamicData.options.feedback_categories);
        //temp.product_group.setList(dynamicData.options.product_group);
        temp.part_knowledge_shared.setList(dynamicData.options.yes_no);
        temp.handedover_product_literature.setList(angular.copy(dynamicData.options.yes_no));
        temp.does_retailer_get_sufficient_meterial.setList(angular.copy(dynamicData.options.yes_no));
        temp.counter_share.setList(dynamicData.options.counter_share);

        //Define the child nodes for the multiselect forms.
        temp.product_group.childNode = temp.part_line;
        temp.part_line.childNode = temp.part_no;

        //Define the parent node for the multiselect forms.
        temp.part_line.parentNode = temp.product_group;
        temp.part_no.parentNode = temp.part_line;

        temp.part_line.validateChecked = function () {
            this.valid = true;
            this.childNode.values = [];

            var _productLine = this;
            this.list.forEach(function (key) {

                for (var i = 0; i < key.items.length; i++) {

                    if (key.items[i].selected == true) {
                        _productLine.valid = true;
                        break;
                    }
                }

                if (i >= key.items.length)
                    _productLine.valid = false;

            });

            if (this.valid) {
                this.errMsg.hide();
                this.collapse();
                this.childNode.expand();
            } else {
                this.errMsg.show("Error: Please select at least one Product Line from each Product Group");
            }
            console.log(this.valid);
            return this.valid;
        }

        //temp.product_group.get();

        if (pg_cache != null) {
            temp.product_group.setNewList(pg_cache);
        }

        scope.form.push(temp);

        // console.log(temp);


    };

    scope.deleteRow = function (rowIndex) {
        scope.form.splice(rowIndex, 1);
    }

    scope.resetRow = function (row) {
		console.log(angular.copy(row));
		row.dirty = false;
		row.counter_share.value = {};
		row.feedback_categories.value = {};
		row.does_retailer_get_sufficient_meterial.value = {};
		row.handedover_product_literature.value = {};
		row.part_knowledge_shared.value = {};

		row.visit_date.value = "";

		row.product_group.expand();
		row.product_group.values = [];
		row.product_group.removeSelected();
		row.part_line.values = [];
		row.part_line.removeSelected();
		row.part_no.values = [];
		row.part_no.removeSelected();
		
		row.feedback_text = "";
		row.no_of_cupon_claimed = "";
		row.no_of_newparts = "";

	}

    s.initiateForm = function () {
        scope.form = [];
        dynamicData.mapped_champoins.forEach(function (champion, index) {
            scope.pushRow(champion);
        });


        //SETTING LIST FOR CACHE INSTEAD OF CALLING SERVER FOR EACH ROW
        if (pg_cache == null) {


            scope.form[0].product_group.get([], function (pgList) {
                pg_cache = pgList;
                console.log("pg_cache");
                console.log(pg_cache);

                for (var i = 1; i < scope.form.length; i++) {
                    scope.form[i].product_group.setNewList(pg_cache);
                }

            });
        }
    }


    function VisitDate() {
        this.value = null;
        //DISABLE ANY DATES EARLIER THAN 10 DAYS AGO
        this.init = function (dateElem) {
            var tenDaysAgo = new Date();
            tenDaysAgo.setDate(tenDaysAgo.getDate() - 9);
            dateElem.setSensitiveRange(tenDaysAgo, (new Date()));
        }
    }

    //CONSTRUCTOR FUNCTION FOR THE MULTISELECTOR FORM
    function Multiselect(apiUrl, expanded, keyName) {

        var _self = this,
            timeout = null;


        this.searchTerm = "";
        this.valid = false;
        this.list = [];
        this.values = [];
        this.keyName = keyName || "";
        this.itemsListLinear = [];
        this.apiUrl = apiUrl || '';
        this.childNode = null;
        this.parentNode = null;
        this.disabled = false;
        this.expanded = (expanded == undefined) ? true : expanded;

        this.expand = function () { this.expanded = true }
        this.collapse = function () { this.expanded = false }

        this.errMsg = new Toggle();




        this.setNewList = function (list) {

            _self.list = angular.copy(list);		//Assign dynamic data items to the list array.
            //console.log(success.data);
            _self.values = [];				//Reset the value
            _self.itemsListLinear = [];
            s.loading.hide();				//Hide loading animation
            //_self.expand();
            if (_self.parentNode != null)
                _self.parentNode.collapse();
            // console.log(_self);

            _self.expand();

            if (_self.childNode != null)
                _self.childNode.resetValues();

            //Create a linear list array to feed in the autocomplete search box.
            if (_self.keyName == "part_no") {
                _self.list.forEach(function (key) {
                    if (key.items != undefined) {
                        _self.itemsListLinear = _self.itemsListLinear.concat(key.items);
                    }
                });
            }
        }


        //GET DYNAMIC DATA FOR MULTISELECT ITEMS
        this.get = function (data, callbackData) {


            s.loading.show();

            $http({
                url: _self.apiUrl,
                data: data || [],
                method: "POST",
                headers: {
                    'Content-Type': 'aplication/json'
                }
            }).then(
                function (success) {
                    //Copied
                    _self.setNewList(success.data);

                    if (callbackData != undefined)
                        callbackData(success.data);

                },
                function (failed) {
                    s.loading.hide();
                }
                );
        }


        //SEARCHING FOR THE SERVER FOR PART NUMBER.
        this.serverSearch = function (request, response) {

            //SENDING PARENT VALUES IN ORDER TO RETURN THE RIGHT ITEMS. 
            //ALSO SENDING THE ITEMS WHICH ARE CURRENTLY IN SELECTION SO THAT SERVER DO NOT SEND THEM AGAIN TO AVOID DUBLICATE VALUES
            //console.log(this.searchTerm);

            clearTimeout(timeout);


            timeout = setTimeout(function () {
                if (_self.searchTerm.length <= 2) {
                    console.log(_self.searchTerm);
                    _self.list = [];
                    return;
                }

                $http({
                    url: _self.apiUrl,
                    params: {
                        searchterm: _self.searchTerm
                    },
                    data: {
                        parentValues: _self.parentNode.values,
                        selected: _self.values
                    },
                    method: "POST",
                    headers: {
                        'Content-Type': 'aplication/json'
                    }
                }).then(
                    function (success) {
                        if (response != undefined)
                            response(success.data);

                        //console.log(_self.values);
                        //console.log(success.data);

                        //CHECKING FOR THE OPTIONS WHICH ARE ALREADY SELECTED
                        var dublicate = false;
                        _self.list = [];

                        success.data.forEach(function (partNo, index) {
                            dublicate = false;
                            for (var i = 0; i < _self.values.length; i++) {
                                if (partNo.id == _self.values[i].id) {
                                    dublicate = true;
                                }
                            }
                            if (dublicate === false)
                                _self.list.push(partNo);
                        });



                    },
                    function (failed) { }
                    );
            }, 200);


            //response(data);
        }

        //WHEN USER SELECT THE ITEM BY CLIKING ON THE ITEMS
        this.select = function (x) {

            if (x.selected == true)
                return;

            this.values.push(x);
            x.selected = true;
            //console.log(x);
        }

        //WHEN USER DELETE THE ITEM BY CLICKING THE X BUTTON
        this.deselect = function (index) {
            this.values[index].selected = false;
            this.values.splice(index, 1);
            //console.log(index);
        }

        this.disable = function () {
            if (!this.disabled)
                this.disabled = true;
        }

        this.enable = function () {
            if (this.disabled)
                this.disabled = false;
        }

        this.resetValues = function () {
            this.values = [];
            this.list = [];
            this.collapse();
        }

        //RESET ALL SELECTED FLAGS KEYS ASSIGNED ON THE ITEMS
        this.removeSelected = function () {
            this.list.forEach(function (key) {
                if (key.selected)
                    key.selected = false;
            });
        }

        this.expand = function () { this.expanded = true };
        this.collapse = function () { this.expanded = false };
        this.toggleExpansion = function () { this.expanded = !this.expanded };

    }

    //FILTER FUNCTION BY STRING
    scope.filterString = function (str) {

        return function (item) {
            if (str == '' || str == null)
                return item;
            else if (item.name.toLowerCase().indexOf(str.toLowerCase()) != -1)
                return item;
        }

    }




    //SUBMITTING INPUT FORM FINALLY

    scope.submitForm = function (event) {
        event.preventDefault();
        console.log(scope.form);

        var formData = [];

        scope.form.forEach(function (key, index) {
            if (key.dirty) {
                formData.push({
                    area_office: key.area_office,
                    counter_share: key.counter_share.value,
                    distributor_code: key.area_office,
                    distributor_name: key.distributor_name,
                    does_retailer_get_sufficient_meterial: key.does_retailer_get_sufficient_meterial.value,
                    feedback_categories: key.feedback_categories.value,
                    garage_name: key.garage_name,
                    guru_id: key.guru_id,
                    guru_name : key.guru_name,
                    handedover_product_literature: key.handedover_product_literature.value,
                    lastVisit_date: key.lastVisit_date,
                    last_visit_feedback: key.last_visit_feedback,
                    part_knowledge_shared: key.part_knowledge_shared.value,
                    part_no: key.part_no.values,
                    product_group: key.product_group.values,
                    part_no: key.part_no.values,
                    part_line: key.part_line.values,
                    visit_date: key.visit_date.value,
                    no_of_newparts: key.no_of_newparts,
                    no_of_cupon_claimed: key.no_of_cupon_claimed,
                    //part_description: key.part_description,
                    feedback_text: key.feedback_text

                });
            }
        });

        if (formData.length <= 0) {
            s.formMessage.showMsg("Form is empty");
            return;
        }


        var dublicateVisitDate = false;

        for (var i = 0; i < formData.length; i++) {
            for (var j = (i + 1); j < formData.length; j++) {
                if (formData[i].guru_id == formData[j].guru_id && formData[i].visit_date.getDate() == formData[j].visit_date.getDate()) {
                    dublicateVisitDate = true;
                    console.log("Dublicate found");
                    console.log(formData[i]);
                    console.log(formData[j]);
                    break;
                }


            }

            if (dublicateVisitDate === true)
                break;
        }

        if (dublicateVisitDate === true) {
            s.formMessage.showMsg("Please correct dublicate visit dates for " + formData[i].guru_name);
            return;
        }


        console.log("Harnessed dat");
        console.log(formData);

        scope.loading.show("Submiting your inputs");


        $http({
            url: s.api.formAction,
            data: formData,
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(
            function (response) {
                console.log(response.data);
                console.log("Success");
                scope.loading.hide();
                s.postSubmit.show();
                s.initiateForm();
            },
            function (failed) {
                console.log("Failed due to errors");
                scope.loading.hide();
            }
            );
    }


    s.initiateForm();

}]);