<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');
	
	$data = array();
	
	for($i = 0; $i < 10; $i++){
		array_push($data, array(
			"country "=> "AFGHAN",
			"id" => $i
		));
	}
	
	echo json_encode($data);
	
	
?>