<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');
	
	
	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		$data = json_decode(file_get_contents('php://input'), true);
		$match = $data['match'];
		$value = $data['value'];
		//$name	= $value['type'];
		$id	= $value['id'];
		
		$output = array();
		
		$j = 0;
		$msrDsr = ["msr","dsr","both"];
		
		$name = "DUmmy";
		
		if($match == "region"){
			$name = "APM_";
		} else if($match == "apm"){
			$name = "Dist_";
		} else if($match == "distributor"){
			$name = "Code_";
		}
		
		if($match != "distributor"){
			for($i = 0; $i < 20; $i++){
				array_push($output,array(
					"type" => $name."_".$i.$name,
					"name" => $name."_".$i.$name,
					"id"	=> $i			
				));
			}
		} else {
			for($i = 0; $i < 20; $i++){
				
				if($j == 3)
					$j = 0;
				
				array_push($output,array(
					"type" => $name."_".$i.$name,
					"name" => $name."_".$i.$name,
					"id"	=> $i,
					"msrDsr" => $msrDsr[$j]
				));
				
				$j++;
			}
		}
		
		
		
		//sleep(1);
		echo json_encode($output);
		
	}
	
?>